// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyAWed7HhrbpjAUK0Ba16-xxPafzwXe7V70",
    authDomain: "chat-6a4e5.firebaseapp.com",
    projectId: "chat-6a4e5",
    storageBucket: "chat-6a4e5.appspot.com",
    messagingSenderId: "714655662035",
    appId: "1:714655662035:web:63d10f3c4bf96dff5df926",
    measurementId: "G-F4XK70MN75"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
