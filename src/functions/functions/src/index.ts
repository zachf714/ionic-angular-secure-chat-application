/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/no-empty-function */
import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

admin.initializeApp();

export const createPush = functions.firestore
    .document("/groups/{id}/messages/{msgID}")
    .onCreate((snap, context) => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      let msg: any = null;

      if (snap) {
        msg = snap.data();
      }

      const groupId = context.params.id;

      console.log("msg: ", msg);
      console.log("group: ", groupId);

      return admin.firestore().doc(`groups/${groupId}`).get()
          .then((group) => {
            console.log("group data: ", group.data());
            let members = [];
            // eslint-disable-next-line prefer-const
            let data = group.data();
            if (data) {
              members = data["users"];
            }

            // eslint-disable-next-line prefer-const
            for (let mem of members) {
              if (mem.id != msg.from) {
                console.log("Member ID: ", mem.id);
                admin.firestore().doc(`users/${mem.id}`).get().then((data) => {
                  const userData = data.data();
                  console.log(userData);
                  let dndStatus = null;
                  if (userData) {
                    dndStatus = userData["dndStatus"];
                  } else {
                    dndStatus = "disabled";
                  }
                  if ( dndStatus !== "active" ) {
                    console.log(mem);
                    console.log(mem.dndStatus);
                    const payload = {
                      notification: {
                        title: "New Message",
                        body: msg.msg,
                      },
                      data: {
                        chat: groupId,
                      },
                    };
                    console.log("send data: ", payload);
                    admin.firestore().doc(`devices/${mem.id}`).get()
                        .then((device) => {
                          // eslint-disable-next-line prefer-const
                          let deviceData = device.data();
                          if (deviceData) {
                            // eslint-disable-next-line prefer-const
                            let token = deviceData["token"];
                            console.log("send to: ", token);
                            // eslint-disable-next-line max-len
                            admin.messaging().sendToDevice(token, payload).then(() => {
                              console.log("PUSH SENT");
                            }, (err) => {
                              console.log("push err: ", err);
                            });
                          }
                        }, (err) => {
                          console.log("device err: ", err);
                        });
                  }
                });
              }
            }
          }, (err) => {
            console.log("groups err: ", err);
          });
    });

export const removeGroup = functions.firestore
    .document("/groups/{id}")
    .onDelete((snap, context) => {
      const groupId = context.params.id;
      const groupData = snap.data();

      console.log("group: ", groupId);

      const users = groupData.users;

      // eslint-disable-next-line prefer-const
      for (let user of users) {
        if (user.id != null) {
          console.log("User ID: ", user.id);
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          admin.firestore().doc(`users/${user.id}`).collection("groups").get().then( (querySnapshot) => {
            querySnapshot.docs.forEach((doc) => {
              // eslint-disable-next-line prefer-const
              let data = doc.data();
              console.log("User Data: ", data);
              if (data.id == groupId) {
                admin.firestore().doc(`users/${user.id}/groups/${doc.id}`).delete().then(function() {
                  console.log("Group Removed from User");
                });
              }
            });
          });
        }
      }
    });
