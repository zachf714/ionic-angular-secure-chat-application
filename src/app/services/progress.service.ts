import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ProgressService {

  constructor(private loadingController: LoadingController) { }
  // Show the loader for infinite time
  showLoader() {

    this.loadingController.create({
      message: 'Processing'
    }).then((res) => {
      res.present();
    });

  }

  // Hide the loader if already created otherwise return error
  hideLoader() {
    this.loadingController.dismiss().then((res) => {
      console.log('Complete!', res);
    }).catch((error) => {
      console.log('error', error);
    });

  }


}
