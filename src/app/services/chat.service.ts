import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { take, map, switchMap } from 'rxjs/operators';
import * as firebase from 'firebase/app';
import { forkJoin, from } from 'rxjs';
import { AngularFireStorage, AngularFireStorageReference } from '@angular/fire/storage';
import { fileURLToPath } from 'url';
 
@Injectable({
  providedIn: 'root'
})
export class ChatService {

  data: any;
 
  constructor(private db: AngularFirestore, private auth: AuthService, private storage: AngularFireStorage) { }
 
  findUser(value) {
    let email = this.db.collection('users', ref => ref.where('email', '==', value)).snapshotChanges().pipe(
      take(1),
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as {};
        const id = a.payload.doc.id;
        console.log(data);
        return { id, ...data };
      }))
    );
    let username = this.db.collection('users', ref => ref.where('username', '==', value)).snapshotChanges().pipe(
      take(1),
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as {};
        const id = a.payload.doc.id;
        console.log(data);
        return { id, ...data };
      }))
    );
    return [email, username];
  }
 
  createGroup(title, users, filepath) {
    let current = {
      email: this.auth.currentUser.email,
      id: this.auth.currentUserId,
      username: this.auth.username
    };
 
    let allUsers = [current, ...users];
    return this.db.collection('groups').add({
      title: title,
      grouppicture: filepath,
      users: allUsers
    }).then(res => {
      let promises = [];
 
      for (let usr of allUsers) {
        let oneAdd = this.db.collection(`users/${usr.id}/groups`).add({
          id: res.id
        });
        promises.push(oneAdd);
      }
      return Promise.all(promises);
    })
  }
 
  getGroups() {
    console.log('Currrent UserID: ', this.auth.currentUserId);
    return this.db.collection(`users/${this.auth.currentUserId}/groups`).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const user_group_key = a.payload.doc.id;
        try{
          return this.getOneGroup(data['id'], user_group_key);
        } catch (err) {
          console.log(err);
        }
      }))
    )
  }
 
  getOneGroup(id, user_group_key = null) {
        return this.db.doc(`groups/${id}`).snapshotChanges().pipe(
          take(1),
          map(changes => {
            const data = changes.payload.data() as {};
            console.log('Group Data: ', data);
            const group_id = changes.payload.id;
            return { user_group_key, id: group_id, ...data };
          })
        )
  }
 
  getChatMessages(groupId) {
    return this.db.collection(`groups/${groupId}/messages`, ref => ref.orderBy('createdAt')).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as {};
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    )
  }
 
  addChatMessage(msg, chatId) {
    return this.db.collection('groups/' + chatId + '/messages').add({
      msg: msg,
      from: this.auth.currentUserId,
      createdAt: firebase.firestore.FieldValue.serverTimestamp()
    });
  }
 
  addFileMessage(file, chatId) {
    let newName = `${new Date().getTime()}-${this.auth.currentUserId}.png`;
    let storageRef: AngularFireStorageReference = this.storage.ref(`/files/${chatId}/${newName}`);
    return {task: storageRef.putString(file, 'base64', { contentType: 'image/png'}), ref: storageRef };
  }
 
  saveFileMessage(filepath, chatId) {
    return this.db.collection('groups/' + chatId + '/messages').add({
      file: filepath,
      from: this.auth.currentUserId,
      createdAt: firebase.firestore.FieldValue.serverTimestamp()
    });
  }

  addFileGroupPicture(file, userID) {
    let newName = `${new Date().getTime()}-${userID}.png`;
    let storageRef: AngularFireStorageReference = this.storage.ref(`/files/${userID}/${newName}`);
    return {task: storageRef.putString(file, 'base64', { contentType: 'image/png'}), ref: storageRef };
  }

  returnGroupPicture(filepath, groupID) {
    return this.db.collection('groups').doc(groupID).update({'grouppicture' : filepath});
  }
 
  leaveGroup(groupId, users) {
    return this.getGroups().pipe(
      switchMap(userGroups => {
        return forkJoin(userGroups);
      }),
      map(data => {
        let toDelete = null;
 
        for (let group of data) {
          if (group.id == groupId) {
            toDelete = group.user_group_key;
          }
        }
        return toDelete;
      }),
      switchMap(deleteId => {
        return from(this.db.doc(`users/${this.auth.currentUserId}/groups/${deleteId}`).delete())
      }),
      switchMap(() => {
        return from(this.db.doc(`groups/${groupId}`).update({
          users: users
        }));
      })
    );
  }

  updateGroupName(chatID, name){
    return this.db.collection('groups').doc(chatID).update({'title' : name});
  }

  async getUser(userData){
    return this.findUser(userData);
  }

  async addNewUser(userData, chatID, users){
    console.log(userData)
    await this.getUser(userData).then(user =>
      forkJoin(user).subscribe(res => {
        console.log("data: ", res);
        for (let data of res){
          if (data.length > 0) {
            users.push(data[0]);
            this.db.collection('groups').doc(chatID).update({'users' : users});
          }
        }
        userData= '';
      }));
  }

  async deleteGroup(chatID){
    return this.db.collection('groups').doc(chatID).delete().then(function() {
      console.log("Group successfully deleted!");
    });
  }

  removeChatMessage(messageID, chatID){
    return this.db.collection('groups').doc(chatID).collection('messages').doc(messageID).delete()
  }
}