import { Injectable } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed,
  Capacitor
} from '@capacitor/core';
import { Router } from '@angular/router';
import { FCM } from 'capacitor-fcm';
 
const { PushNotifications } = Plugins;
 
@Injectable({
  providedIn: 'root'
})
export class NotificationServiceService {
 
  constructor(private router: Router, private afs: AngularFirestore, private auth: AuthService, private fcm: FCM) { }
 
  initPush() {
    if (Capacitor.platform !== 'web') {
      this.registerPush();
    }
  }

  private saveToken(token) {
    if (!token) return;
 
    const devicesRef = this.afs.collection('devices');
 
    const data = {
      token,
      userId: this.auth.currentUserId
    };
 
    return devicesRef.doc(this.auth.currentUserId).set(data);
  }
 
  private registerPush() {
    PushNotifications.requestPermission().then((permission) => {
      if (permission.granted) {
        // Register with Apple / Google to receive push via APNS/FCM
        PushNotifications.register();
      } else {
        // No permission for push granted
      }
    });
 
    PushNotifications.addListener(
      'registration',
      (token: PushNotificationToken) => {
        console.log('My token: ' + JSON.stringify(token));
        this.fcm.getToken().then((result) => {
          this.saveToken(result.token);
          console.log('FCM Token: ', result.token);
        })
      }
    );
 
    PushNotifications.addListener('registrationError', (error: any) => {
      console.log('Error: ' + JSON.stringify(error));
    });
 
    PushNotifications.addListener(
      'pushNotificationReceived',
      async (notification: PushNotification) => {
        console.log('Push received: ' + JSON.stringify(notification));
        const data = notification.notification.data;
        console.log(data.chat);
      }
    );
 
    PushNotifications.addListener(
      'pushNotificationActionPerformed',
      async (notification: PushNotificationActionPerformed) => {
        const data = notification.notification.data;
        console.log('Action performed: ' + JSON.stringify(notification.notification));
        if (data.chat) {
          console.log(data.chat);
          this.router.navigateByUrl(`/chat/${data.chat}`)
        }
      }
    );
  }
}