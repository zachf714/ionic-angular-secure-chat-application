import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import * as auth from 'firebase/app'
import 'firebase/firestore';
import 'firebase/auth'
import { take, map, tap } from 'rxjs/operators';
import { Plugins } from '@capacitor/core';
import '@codetrix-studio/capacitor-google-auth';
import { Platform } from '@ionic/angular';
import { AngularFireStorage, AngularFireStorageReference } from '@angular/fire/storage';

 
export interface UserCredentials {
  username: string;
  email: string;
  password: string;
  phone: string;
  profilePicture: string;
  status: string;
  dndStatus: string;
}
 
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: User = null;
  username = '';
  profilePicture = '';
  status = '';
  phone = '';
  biometricActivationStatus = '';
  dndStatus = '';
 
  //Authentication Service Constructor
  constructor(private afAuth: AngularFireAuth, private db: AngularFirestore, private storage: AngularFireStorage) {
    this.afAuth.onAuthStateChanged((user: User) => {
      this.user = user;
      console.log('persistant user: ', this.user);
      if (this.user) {
        this.db.doc(`users/${this.user.uid}`).valueChanges().pipe(
          tap(res => {
            console.log('Is Authed')
            this.username = res['username'];
            this.profilePicture = res['profilePicture']
            this.status = res['status']
            this.phone = res['phone']
            console.log(this.profilePicture);
          })
        ).subscribe();
      }
    })
   }
 
   //Signup with Firebase Provider
  signUpEmail(credentials: UserCredentials) {
    return this.afAuth.createUserWithEmailAndPassword(credentials.email, credentials.password)
      .then((data) => {
        return this.db.doc(`users/${data.user.uid}`).set({
          username: credentials.username,
          email: data.user.email,
          dndStatus: 'disabled',
          created: firebase.firestore.FieldValue.serverTimestamp()
        });
      });
  }
 
  //Login with Firebase Provider
  signInEmail(credentials: UserCredentials) {
    return this.afAuth.signInWithEmailAndPassword(credentials.email, credentials.password);
  }

  //Google SSO Support
  async signInWithGoogleCredentials() {
    let googleUser = await Plugins.GoogleAuth.signIn() as any;
    console.log('My User: ', googleUser);
    const credential = firebase.auth.GoogleAuthProvider.credential(googleUser.authentication.idToken);
    console.log(credential);
    this.afAuth.onAuthStateChanged((user) => {
      this.user = user;
      if (this.user) {
        console.log("user: ", this.user);
          this.db.doc(`users/${this.currentUserId}`).update({
          username: this.user.displayName,
          email: this.user.email,
          profilePicture: this.user.photoURL,
          phone: this.user.phoneNumber,
          created: firebase.firestore.FieldValue.serverTimestamp()
        });
        this.setLocalStorage();
      }
    })
    return this.afAuth.setPersistence(firebase.auth.Auth.Persistence.LOCAL)
      .then(() => {
        return this.afAuth.signInWithCredential(credential);
      }) .catch(err => {
        return Promise.reject(err);
      })
  }

  //Add Apple SSO/OAuth Support
  async signInWithAppleCredentials(appleIDResponse){
    //Create a new custom Oauth Provider for SSO
    const provider = new firebase.auth.OAuthProvider('apple.com');
    //Store Apple ID Credential Toke Temp
    const ssoCredential = provider.credential({
      idToken: appleIDResponse.identityToken
    });
    //Authenticate FireAuth with Credential
    const credential = this.afAuth.signInWithCredential(ssoCredential);
    //Get Authentication State Change
    this.afAuth.onAuthStateChanged((user) => {
      this.user = user;
      if (this.user && appleIDResponse.email) {
        console.log("user: ", this.user);
        this.db.doc(`users/${this.currentUserId}`).update({
        username: appleIDResponse.givenName,
        email: appleIDResponse.email,
        created: firebase.firestore.FieldValue.serverTimestamp()
        });
        this.setLocalStorage();
      }
    })
    //Set Local Storatge Persistance
    return this.afAuth.setPersistence(firebase.auth.Auth.Persistence.LOCAL)
    .then(() => {
      //Return the login
      return this.afAuth.signInWithCredential(ssoCredential);
    }) .catch(err => {
      return Promise.reject(err);
    })
  }
 
  signOut() {
    return this.afAuth.signOut();
  }
 
  resetPw(email) {
    return this.afAuth.sendPasswordResetEmail(email);
  }
 
  updateUser(username) {
    return this.db.doc(`users/${this.currentUserId}`).update({
      username
    });
  }

  addFile(file) {
    let newName = `${new Date().getTime()}-${this.currentUserId}.png`;
    let storageRef: AngularFireStorageReference = this.storage.ref(`/files/$${this.currentUserId}/${newName}`);
    return {task: storageRef.putString(file, 'base64', { contentType: 'image/png'}), ref: storageRef };
  }

  // async setProfileImage(filepath) {
  //     const profile = {
  //         displayName: this.user.displayName,
  //         photoURL: filepath
  //     }
  //     return (await this.afAuth.currentUser).updateProfile(profile);
  // }

  async setProfileImage(profilePicture) {
    return (await this.db.doc(`users/${this.currentUserId}`).update({
      profilePicture
    }));
}

  async setDisplayName(username) {
    return (await this.db.doc(`users/${this.currentUserId}`).update({
      username
    }));
}

  async setUserStatus(status){
    return (await this.db.doc(`users/${this.currentUserId}`).update({
      status
    }));
  }

  async setUserPhone(phone){
    return (await this.db.doc(`users/${this.currentUserId}`).update({
      phone
    }));
  }

  async setDNDStatus(dndStatus){
    return (await this.db.doc(`users/${this.currentUserId}`).update({
      dndStatus
    }));
  }

  async setLocalStorage(){
    await Plugins.Storage.set({
      key: 'user',
      value: JSON.stringify(this.user) });
  }

  async setBiometricLocalStorage(biometricValue){
    await Plugins.Storage.set({
      key: 'biometric',
      value: biometricValue
    })
  }

  async getLocalBiometric(){
    const ret = await Plugins.Storage.get({ key: 'biometric' });
    return ret.value
  }
 
  async getLocalStorage(){
    const ret = await Plugins.Storage.get({ key: 'user' });
    this.user = JSON.parse(ret.value);
  }

  get authenticated(): boolean {
    return this.user !== null;
  }
 
  get currentUser(): any {
    return this.authenticated ? this.user : null;
  }
 
  get currentUserId(): string {
    console.log(this.user.uid);
    return this.authenticated ? this.user.uid : '';
  }

  get currentUserImage(): string{
      this.db.doc(`users/${this.user.uid}`).valueChanges().pipe(
        tap(res => {
          this.profilePicture =  res['profilePicture']
          console.log(this.profilePicture);
        })
      )
  return this.profilePicture;
}

get biometricStatus(): string{
  this.db.doc(`users/${this.user.uid}`).valueChanges().pipe(
    tap(res => {
      this.biometricActivationStatus =  res['biometricStatus']
      console.log(this.biometricActivationStatus);
    })
  )
return this.biometricActivationStatus;
}

currentUserStatus(){
  this.db.doc(`users/${this.user.uid}`).valueChanges().pipe(
    tap(res => {
      this.status =  res['status']
      console.log(this.status);
    })
  ).subscribe();
return this.status;
}

currentUserPhone(){
  this.db.doc(`users/${this.user.uid}`).valueChanges().pipe(
    tap(res => {
      this.phone =  res['phone']
      console.log(this.phone);
    })
  ).subscribe();
return this.phone;
}

currentUserName(){
  this.db.doc(`users/${this.user.uid}`).valueChanges().pipe(
    tap(res => {
      this.username =  res['username']
      console.log(this.username);
    })
  ).subscribe();
return this.username;
}

currentProfileImage(){
  this.db.doc(`users/${this.user.uid}`).valueChanges().pipe(
    tap(res => {
      this.profilePicture =  res['profilePicture']
      console.log("This Profile Picture: ", this.profilePicture);
    })
  ).subscribe();
return this.profilePicture;
}

  setUser(user){
    return this.user = user;
  }
}
