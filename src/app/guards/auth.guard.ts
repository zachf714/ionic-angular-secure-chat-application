import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take, map, tap } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
 
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
 
  constructor(private router: Router, private afAuth: AngularFireAuth, private auth: AuthService) { }

  canActivate(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.afAuth.onAuthStateChanged((user: firebase.User) => {
        if (user) {
          console.log(user);
          resolve(true);
        } else {
          console.log('User is not logged in');
          this.router.navigate(['/login']);
          resolve(false);
        }
      });
    });
  }
}