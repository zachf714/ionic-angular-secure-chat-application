import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { forkJoin } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { ChatService } from 'src/app/services/chat.service';
import { ProgressService } from 'src/app/services/progress.service';

@Component({
  selector: 'app-start-chat',
  templateUrl: './start-chat.page.html',
  styleUrls: ['./start-chat.page.scss'],
})
export class StartChatPage implements OnInit {

  users = [];
  title = '';
  grouppicture = null;
  aUser = '';
  storedPicture = null;
  profilePicture = '';

  constructor(private chatService: ChatService, private router: Router, private camera: Camera, private auth: AuthService, private progress: ProgressService) { }

  ngOnInit() {
    this.profilePicture = this.auth.currentProfileImage();
  }

  addUser() {
    let obs = this.chatService.findUser(this.aUser);

    console.log(this.aUser);

    forkJoin(obs).subscribe(res => {
      console.log("data: ", res);
      for (let data of res){
        if (data.length > 0) {
          this.users.push(data[0]);
        }
      }
      this.aUser= '';
    });
  }

  getPicture() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.ALLMEDIA,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      correctOrientation: true
    }

    this.camera.getPicture(options).then(data => {
      this.displayLoader();
      let obj = this.chatService.addFileGroupPicture(data, this.auth.currentUserId);
      let task = obj.task;

      task.then(res => {
        obj.ref.getDownloadURL().subscribe(url => {
          this.grouppicture = url;
          this.hideLoader();
        })
      })
      task.percentageChanges().subscribe(change => {
        console.log('change: ', change);
  })
    });
  }

 setPicture(){
    let obj = this.chatService.addFileGroupPicture(this.storedPicture, this.auth.currentUserId);
      let task = obj.task;

      task.then(res => {
        obj.ref.getDownloadURL().subscribe(url => {
          this.grouppicture = url;
        })
      })
      task.percentageChanges().subscribe(change => {
        this.displayLoader();
        console.log('change: ', change);
  })
  this.hideLoader();
  return this.grouppicture;
}

  createGroup() {
        console.log(this.grouppicture);
        this.chatService.createGroup(this.title,this.users,this.grouppicture).then(() => {
        this.router.navigateByUrl('/tabnavigation')
    })
  }

  displayLoader(){
    this.progress.showLoader();
  }
  
  hideLoader(){
    this.progress.hideLoader();
  }

}
