import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AuthService } from './../../services/auth.service'
import { Observable } from 'rxjs';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ProgressService } from 'src/app/services/progress.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  constructor(private auth: AuthService, private router: Router, private alertController: AlertController, private camera: Camera, private progress: ProgressService) { }
  
  groups: Observable<any>;
  profilePicture = this.auth.currentProfileImage();
  displayName = '';
  userEmail = '';
  userPhone = '';
  userStatus = '';
  

  ngOnInit() {
    this.profilePicture = this.auth.currentProfileImage();
    this.displayName = this.auth.currentUserName();
    this.userEmail = this.auth.user.email;
    this.userPhone = this.auth.currentUserPhone();
    console.log(this.userPhone);
    this.userStatus = this.auth.currentUserStatus();
    console.log(this.displayName);
  }

  signOutAlert(){
    this.alertController.create({
      header: 'Log Out',
      subHeader: 'Confirm Logout',
      message: 'Are you sure you want to log out?',
      buttons: [
        {
        text: 'Cancel',
          handler: () => {
            console.log('Cancel');
          }
        },
        {
        text: 'Logout',
          handler: () => {
            this.signOut();
            console.log('Logout');
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }

  signOut(){
    this.auth.signOut().then(() => {
      this.router.navigateByUrl('/login');
    });
  }

  //Update Profile Image
  setProfileImage() { 
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.ALLMEDIA,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      correctOrientation: true
    }

    this.camera.getPicture(options).then(data => {
      this.displayLoader();
      let obj = this.auth.addFile(data);
      let task = obj.task;

      task.then(res => {
        obj.ref.getDownloadURL().subscribe(url => {
          this.auth.setProfileImage(url);
          this.profilePicture = url;
          this.hideLoader();
        })
      })
      task.percentageChanges().subscribe(change => {
        console.log('change: ', change);
      })
    });
  }

  updateStatusPrompt() {
    this.alertController.create({
      header: 'Update Status',
      subHeader: 'Update your Status',
      message: 'Enter New Status',
      inputs: [
        {
          name: 'status',
          placeholder: 'Eg.Loving IonChat',
          
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: (data: any) => {
            console.log('Canceled', data);
          }
        },
        {
          text: 'Done!',
          handler: (data) => {
            console.log('Saved Information', data);
            this.auth.setUserStatus(data.status).then(() => this.userStatus = this.auth.currentUserStatus());
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }

  updatePhonePrompt() {
    this.alertController.create({
      header: 'Update Phone Number',
      subHeader: 'Phone Number',
      message: 'Enter New Phone Number',
      inputs: [
        {
          name: 'phone',
          placeholder: 'Eg.111-222-3344',
          
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: (data: any) => {
            console.log('Canceled', data);
          }
        },
        {
          text: 'Done!',
          handler: (data) => {
            console.log('Saved Information', data);
            this.auth.setUserPhone(data.phone).then(() => this.userPhone = this.auth.currentUserPhone());
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }

  updateDisplayName() {
    this.alertController.create({
      header: 'Update Display Name',
      subHeader: 'Update your Name',
      message: 'Enter New Name',
      inputs: [
        {
          name: 'name',
          placeholder: 'Eg.Loving IonChat',
          
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: (data: any) => {
            console.log('Canceled', data);
          }
        },
        {
          text: 'Done!',
          handler: (data) => {
            console.log('Saved Information', data);
            this.auth.setDisplayName(data.name).then(() => this.displayName = this.auth.currentUserName());
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }

  enableBiometricsAlert(){
    this.alertController.create({
      header: 'Enable/Disable Biometrics',
      subHeader: 'Lock Application with Biometrics',
      message: 'Are you sure you want 2nd stage authentication?',
      buttons: [
        {
        text: 'Disable',
          handler: () => {
            this.auth.setBiometricLocalStorage('disabled');
            console.log('disabled')
          }
        },
        {
        text: 'Enable',
          handler: () => {
            this.auth.setBiometricLocalStorage('active');
            console.log('active');
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }

  enableDNDsAlert(){
    this.alertController.create({
      header: 'Enable/Disable Do Not Disturb',
      subHeader: 'Disable Message Notifications',
      message: 'Are you sure you to disable notifications?',
      buttons: [
        {
        text: 'Disable',
          handler: () => {
            this.auth.setDNDStatus('disabled');
            console.log('disabled')
          }
        },
        {
        text: 'Enable',
          handler: () => {
            this.auth.setDNDStatus('active');
            console.log('active');
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }


  displayLoader(){
    this.progress.showLoader();
  }
  
  hideLoader(){
    this.progress.hideLoader();
  }

}
