import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { ChatService } from 'src/app/services/chat.service';
import { AuthService } from './../../services/auth.service'

@Component({
  selector: 'app-chats',
  templateUrl: './chats.page.html',
  styleUrls: ['./chats.page.scss'],
})
export class ChatsPage implements OnInit {

  constructor(private auth: AuthService, private router: Router, private chatService: ChatService, private alertController: AlertController) { }

  groups: Observable<any>;
  profilePicture = './../assets/images/icon.png';

  ngOnInit() {
    this.groups = this.chatService.getGroups();
    //this.profilePicture = this.auth.currentProfileImage();
    console.log('Chats Page Profile Picture: ', this.profilePicture);
  }
}
