import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabnavigationPageRoutingModule } from './tabnavigation-routing.module';

import { TabnavigationPage } from './tabnavigation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabnavigationPageRoutingModule
  ],
  declarations: [TabnavigationPage]
})
export class TabnavigationPageModule {}
