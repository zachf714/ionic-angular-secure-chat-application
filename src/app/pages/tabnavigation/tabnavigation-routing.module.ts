import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabnavigationPage } from './tabnavigation.page';

const routes: Routes = [
  {
    path: 'tabnavigation',
    component: TabnavigationPage,
    children: [
      {
        path: 'chats',
        children: [
          {
          path: '',
          loadChildren: () => import('../../pages/chats/chats.module').then( m => m.ChatsPageModule),
          }
        ]
        
      },
      {
        path: 'profile',
        children: [
          {
            path: '',
            loadChildren: () => import('../../pages/profile/profile.module').then( m => m.ProfilePageModule),
          }
        ]
      },
    ]
  },
  {
    path: '',
    redirectTo: 'tabnavigation/chats',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabnavigationPageRoutingModule {}
