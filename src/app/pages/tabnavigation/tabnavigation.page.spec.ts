import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabnavigationPage } from './tabnavigation.page';

describe('TabnavigationPage', () => {
  let component: TabnavigationPage;
  let fixture: ComponentFixture<TabnavigationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabnavigationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TabnavigationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
