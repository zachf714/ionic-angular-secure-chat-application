import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { ChatService } from 'src/app/services/chat.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-chat-settings',
  templateUrl: './chat-settings.page.html',
  styleUrls: ['./chat-settings.page.scss'],
})
export class ChatSettingsPage implements OnInit {

  constructor(private modalController: ModalController, private navParams: NavParams, private alertController: AlertController, private chatService: ChatService, private router: Router, private auth: AuthService, private camera: Camera) { }

  modalTitle: string;
  modelId: number;
  chat = null;
  grouppicture = null;

  ngOnInit() {
    console.table(this.navParams);
    this.modelId = this.navParams.data.paramID;
    this.modalTitle = this.navParams.data.paramTitle;
    this.chat = this.navParams.data.chat;
    this.grouppicture = this.chat.grouppicture;
  }

  async closeModal(status) {
    let onClosedData = null;
    if (status != null){
      onClosedData = status;
    } else {
      onClosedData = "Closed";
    }
    await this.modalController.dismiss(onClosedData);
  }

  //Update Profile Image
  updateGroupPicture() { 
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.ALLMEDIA,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      correctOrientation: true
    }

    this.camera.getPicture(options).then(data => {
     let obj = this.chatService.addFileGroupPicture(data, this.auth.currentUserId);
      let task = obj.task;

      task.then(res => {
        obj.ref.getDownloadURL().subscribe(url => {
          this.chatService.returnGroupPicture(url, this.chat.id);
        })
      })
      task.percentageChanges().subscribe(change => {
        console.log('change: ', change);
      })
    });
  }

  updateNamePrompt() {
    this.alertController.create({
      header: 'Update Group Name',
      subHeader: 'Update your Group Name',
      message: 'Enter New Name',
      inputs: [
        {
          name: 'name',
          placeholder: 'Eg.Chat Group',
          
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: (data: any) => {
            console.log('Canceled', data);
          }
        },
        {
          text: 'Done!',
          handler: (data) => {
            console.log('Saved Information', data);
            this.chatService.updateGroupName(this.modelId, data.name);
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }

  leave() {
    let newUsers = this.chat.users.filter(usr => usr.id != this.auth.currentUserId);

    this.chatService.leaveGroup(this.chat.id, newUsers).subscribe(res => {
      this.router.navigateByUrl('/tabnavigation');
    });
  }

  addUserPrompt() {
    this.alertController.create({
      header: 'Add User',
      subHeader: 'Add a New User',
      message: 'Enter Email of User',
      inputs: [
        {
          name: 'email',
          placeholder: 'Eg.zfrank@uncc.edu',
          
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: (data: any) => {
            console.log('Canceled', data);
          }
        },
        {
          text: 'Done!',
          handler: (data) => {
            console.log('Saved Information', data);
            this.chatService.addNewUser(data.email, this.chat.id, this.chat.users);
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }

  showLeaveConfirm() {
    this.alertController.create({
      header: 'Confirmation',
      subHeader: 'Leave Group',
      message: 'Are you sure? you want to leave this group?',
      buttons: [
        {
          text: 'Leave Group',
          handler: () => {
            console.log('Leaving Group');
            console.log(this.chat.users);
            this.leave();
            this.closeModal('left')
          }
        },
        {
          text: 'Cancel',
          handler: () => {
            console.log('Not Deleting');
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }

  showDeleteConfirm() {
    this.alertController.create({
      header: 'Confirmation',
      subHeader: 'Delete Chat',
      message: 'Are you sure? you want to delete this chat?',
      buttons: [
        {
          text: 'Delete Chat',
          handler: () => {
            console.log('Deleting Chat');
            console.log(this.chat.id);
            this.chatService.deleteGroup(this.chat.id);
            this.closeModal('deleted')
          }
        },
        {
          text: 'Cancel',
          handler: () => {
            console.log('Not Deleting');
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }

}
