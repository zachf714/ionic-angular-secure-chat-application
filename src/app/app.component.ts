import { Component, NgZone } from '@angular/core';

import { AlertController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { NotificationServiceService } from './services/notification-service.service';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private afAuth: AngularFireAuth,
    private router: Router,
    private auth: AuthService,
    private notifications: NotificationServiceService,
    private faio: FingerprintAIO
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.afAuth.onAuthStateChanged((user: firebase.User) => {
        if (user) {
          this.auth.setUser(user);
          this.auth.getLocalBiometric().then( result => {
            if (result === 'active'){
              this.faio.isAvailable().then((result: any) => {
                console.log(result)
                this.faio.show({
                  cancelButtonTitle: 'Cancel',
                  description: "Authenticate for Security",
                  disableBackup: false,
                  title: '2nd Stage Authentication',
                  fallbackButtonTitle: 'Use Passcode',
                  subtitle: 'Using Biometrics'
                })
                  .then((result: any) => {
                    console.log(result)
                    this.notifications.initPush();
                    this.router.navigate(['/tabnavigation']);
                  })
                  .catch((error: any) => {
                    console.log(error)
                    alert("Not able to Authenticate")
                  });
              })
                .catch((error: any) => {
                  console.log(error)
                }); 
            } else {
              this.notifications.initPush();
              this.router.navigate(['/tabnavigation']);
            }
          });
        } else {
          console.log('User is not logged in');
          this.router.navigate(['/login']);
        }
      });
    });
  }
}
