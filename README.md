# IonChat

Secure Chat application for iOS based on Ionic Angular frameworks. Uses firebase as a backend Database framework

## Installation and Running

To install this package, follow the link for Apple Testflight: https://testflight.apple.com/join/h63K1KY4

We prefer you install the application from the TestFlight link as opposed to trying to run the application from source, as we have not provided the firebase API key for iOS. The application CAN be run in the browser via Ionic Lab if you install the packages required in the packagelock file. 

```bash
https://testflight.apple.com/join/h63K1KY4
```

## Log In with Test User

```bash
User Email: ionchat@container.tuningsocial.com
User Password: testuser
```

## Test User to Add to Chat

`` bash
Test Email Address to Chat With: adduser@container.tuningsocial.com

## Starting a Chat

```bash

Follow the Plus button on the bottom navigation to start a chat. Add a user by inputting the email address and pressing add user.

A chat image is required to start a chat. Press the Plus button next to the circle to add a new image.

Set your Group name and press the next button in the top bar. You will be forwarded to the home page, and it will refresh.

```

## Using a current chat

```bash
Tap a current chat from the chats page(icon on bottom bar shaped like a chat bubble). 

Send a message by typing in the bottom bar and pressing send. 

Send an image by pressing the plus icon and selecting an image

Delete a message by pressing the trash can button
```

## Chat Settings

```bash
Chat settings are at top of a chat. It is a gear icon.

Chat settings include changing the image, adding a user, Changing the name and leaving.
```

## Profile Page
```bash
Profile Page Settings

Changing profile image from the camera icon next to the profile image

Update display name
Update status
Update Phone Number

Enable/Disable Biometric Vault ( With this enabled for the first time, the next time the app is killed and relaunched, FaceID will ask for permission).

Enable/Disable Do Not Disturb (Enable/Disable notifications from Application)

Logout of Application
```

## UserStories being Executed

```bash
User Stories 1,2,3,4,5,6,7,8,9,10.13,14,15,16,17,18 and 19 are executed. 11 and 12 are being executed in Spring 3.
```

